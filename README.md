[[_TOC_]]

# Project Overview
The KubeMovieAngular project forms part of a Cloud Native initiative aimed to learn, build and share experience in Cloud Native technologies and Dev Ops practices.  

The KubeMovieAngular project is the front-end component of a multi-tier solution to manage a Movie catalog - essentially performing CRUD operations. The Movie catalog is maintained in a back-end repository [KubeMovieStore](https://gitlab.com/cnad/kubedvdstore) and accessible via a RESTful API [KubeMovieController](https://gitlab.com/cnad/kubedvdcontroller).  

The KubeMovieAngular project comprises an Angular Web Application (Angular version 9). The project implements a Git pipeline to build the web application as a Docker image hosted on a Nginx web server and deploys this to a Kubernetes cluster running on an Azure virtual machine.  

The following provides steps for getting setup for Angular development. It also provides the steps followed to scaffold out the initial KubeDvd Angular application.





# KubeMovie

## Getting Starting
<details><summary>Environment Setup</summary>
Pre-requisites are really just Node.js
Either download from [here ](https://nodejs.org/en/download/) or better still download [NVM ](https://github.com/coreybutler/nvm-windows/releases) - which allows easy installation and switching between Node versions, which will be a useful way to ensure all team members are using the same versions.

For instance using an elevated command prompt

```
nvm install 12.8.0
nvm list
nvm use 12.8.0
``` 

The installation of libraries and dependencies are managed using NPM, this is installed as part of Node.  
Use npm to install the angular CLI which will be used subsequently to scaffold out the baseline application.
```
npm install -g @angular/cli
``` 
If not already installed, also install VS Code for use as Angular code editor.
</details>

<details><summary>Cloning, building and running the application</summary>
Open elevated command prompt window.

Clone the project to a local directory. For example.
```
git clone https://gitlab.com/cnad/kubedvdangular.git "C:\Users\briggspa\Documents\Cloud Native\KubeMovieAngular"
```
Now build the application to install all dependencies.
```
cd "C:\Users\briggspa\Documents\Cloud Native\KubeMovieAngular\KubeMovie"
npm install
```
Run the application - this is done from command line in the project directory.
```
cd "C:\Users\briggspa\Documents\Cloud Native\KubeMovieAngular\KubeMovie"
ng serve --open --port 4200
```
The --port setting is optional; port 4200 is default.  
The --open simply opens the url in default browser and is also optional (you would just need to open a browser and url http://localhost:4200/)
</details>

## Requirements
The [KubeMovie](https://gitlab.com/cnad/kubedvdangular/-/tree/master/KubeMovie) Angular application is designed to meet the following basic requirements only:
* Implement UI as per [screen mockups](https://gitlab.com/cnad/kubedvdangular/-/wikis/Screen) and provide the following functionality...
* Listing movie records
* Search movies by title 
* Filter movies by genre
* Add new movie record
* Display movie details
* Edit movie record
* Delete movie record
* Display application settings as required or as a minimum build information

### Screenshots
There are no requirements for a responsive web design to cater for mobile device screen resolutions.  
A minimum resolution of 1024x768 is assumed. 
<details><summary>Show Screenshots (taken at 1024x768 resolution)</summary>

Landing page   
![screenshot-sm-1.png](./README-images/screenshot-sm-1.png)  
Title search  
![screenshot-sm-2.png](./README-images/screenshot-sm-2.png)  
Title search and filtered by genre  
![screenshot-sm-3.png](./README-images/screenshot-sm-3.png)  
View Mode - when movie is selected from list  
![screenshot-sm-4.png](./README-images/screenshot-sm-4.png)  
Edit Mode - when 'edit' button is selected for the currently viewed movie  
![screenshot-sm-5.png](./README-images/screenshot-sm-5.png)  
Add Mode - when 'add' button is selected  
![screenshot-sm-6.png](./README-images/screenshot-sm-6.png)  
Application Setting showing build information   
![screenshot-sm-10.png](./README-images/screenshot-sm-10.png)
</details>


## KubeMovie Dependencies

The KubeMovie application is built using the Angular CLI to scaffold out the application.  
In addition to this boilerplate application the following dependencies were added to provide the functionality described.

### Bootstrap
https://ng-bootstrap.github.io/#/home  
https://www.w3schools.com/bootstrap4/default.asp  

Front-end framework containing collection of reusable code.  
Typically targeted for development of responsive mobile first projects.  
Installed into the project by running the following commands in an elevated command prompt from the project folder.  

```
npm install --save @ng-bootstrap/ng-bootstrap
npm install bootstrap --save
npm install @angular/localize --save
```


### Font Awesome
https://github.com/FortAwesome/angular-fontawesome  

Used for control icons; for example shown here are icons used for search, genre filter, add movie, settings, edit and delete.

![screenshot-sm-8.png](./README-images/screenshot-sm-8.png)  

Installed into the project by running the following commands in an elevated command prompt from the project folder.  

```
npm install @fortawesome/fontawesome-svg-core
npm install @fortawesome/free-solid-svg-icons
npm install @fortawesome/angular-fontawesome@0.6.x

```
### Ngx Avatar
https://www.npmjs.com/package/ngx-avatar  

For generation of avatars seen in Movie list.  

![screenshot-sm-9.png](./README-images/screenshot-sm-9.png)  

Installed into the project by running the following commands in an elevated command prompt from the project folder.  

```
npm install ngx-avatar --save
```

### Ngx Virtual Scroller
https://www.npmjs.com/package/ngx-virtual-scroller  

Used to manage the contents of the Movie list. The KubeMovieController returns the list of movies in manageable chunks (pages). This scroller is used to retrieve subsequent pages when scrolling to the next pages and also to manage and limit the rendering of list items in view.  

Installed into the project by running the following commands in an elevated command prompt from the project folder.  
```
npm install ngx-virtual-scroller
```

### TSLint (Lint for TypeScript) 
https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin  

Not really a dependency to build the project, but as a development aid.
TSLint will pick and fix potential problems and ensure we are using same styling (e.g. using semicolons which are optional).
Install via VS Code...  
View->Extensions and search for TSLint (at the time of writing v1.2.3)

## UI Panel Layout

The following provides a high-level block design of the UI layout to implement the screen design specified [here](https://gitlab.com/cnad/kubemovieangular/-/wikis/Screen)

![panel-layout.png](./README-images/panel-layout.png)


### Navbar
The navbar provides the interface to web application pages. In the initial version there is only one page - the Movies, but this would be the place to provide hooks to additional pages. For the purpose of this first version is offers little more than a title for the landing page and could have been left off. However, most applications would require the need for such an interface so felt worthwhile including this to provide an example of application containing a navbar. This also provides a location from which additional services and/or can be included at a later time. In fact this was used as a container for the theme toggle button on the far right; to allow toggling between light and dark themes.   

### Main Application Area
The main application area is organised using Bootstrap's Grid System, comprising a hierarchy of rows and columns as represented by the mid-grey (row) and light-grey (columns) in the panel layout diagram above. The Grid System allows up to 12 columns across the page. The main application area is split in proportions of 9 for left portion and 3 for the right portion. The left portion contains a row of controls and a row for the movie list. The right portion contains a column (of width 3) for the selected movie's information and also used when creating a new movie.

The implementation of this layout can be found in movie-catalog.component.html  

## Code and main components to get started
### VSCode Folder Structure

The following additional folders (to those scaffolded out) have been created under the src folder to organise project components for KubeMovie
* nav - contains the navbar
* movies - contains the movie related components and services
* common - contains common components that are not necessarily project specific. For example, it contains the implementation of the toast service which could be used on other projects.
* shared - contains shared components and services, which are typically project specific, but used across functional areas
* theme - contains the light and dark themes and associated service to allow toggling themes  

The application folder app contains much of the startup bootstrap wiring.  

Briefly - the startup sequence is something like this:  
* main.ts gets loaded on application start, 
* this will in turn load AppModule (app.module.ts), as AppModule is specified as the bootstrap module in main.ts
* this now makes Angular aware of AppComponent (from app.module.ts - `bootstrap: [AppComponent]`)
* then index.html is loaded, which loads AppComponent (see reference to app-root in index.html, which is the selector for AppComponent)
* AppComponent uses template app.component.ts with html that hooks into our application components; app-nav-bar and router-outlet, which is a placeholder that Angular will dynamically fill based on router state. 
* The RouterModule routes are implemented in app-routing.ts, here we see the empty route is implemented by the MovieCatalogComponent. 
Also note the use of a resolver here - this is used to navigate to the route once the resolver is complete, which we use here to wait for the data to be pulled from the Web API before navigating to the route - the MovieCatalogComponent.  

In summary when the application starts the MovieCatalogComponent is loaded getting input via the movies @Input by the movie-resolver.service, which pulls this data from the backend repository via the REST API.

### MovieCatlogComponent
This is the main component of the application. It provides the page layout (refer movie-catalog.component.html) and handling of user triggered events (movie-catalog.component.ts).  

### Population of the movie list
getMovies() uses the MovieService (movie.service.ts) to fill the table of Movies. The current settings for search term and genre filter are used in the call.  
getMovies() is invoked whenever there is a change to the search term or filter and is used to retrieve the first page of data.  

Note also the functionally similar fetchNextChunk() method. This is triggered by the virtual scroller of the Movie table to load subsequent pages when scrolling to the end. The web api uses a paged retrieval pattern to improve performance by allowing only manageable chunks of data to be retrieved in one call. So this mechanism works with that pattern to retrieve data in chunks as the user scrolls down to look for further data.

Adding, editing and deleting Movies are also handled in this component, although triggered by the user from the AddMovieComponent.  

### AddMovieComponent
Originally just implemented to add movies (hence the name), this later catered for viewing and editing Movies owing to the similar functionality and UI.
AddMovieComponent (which has selector app-add-movie) is selected into the MovieCatalogComponent using the selector from the movie-catalog.component.html.
Also, note the use of `*ngIf` structural directive surrounding the `<add-add-movie>` to only render the template if in add, edit or view mode. If not in these modes then the default 'Stay Entertained' banner is displayed. 

```
<app-add-movie 
	[(addMode)]="addmode" [(viewMode)]="viewmode" [(editMode)]="editmode" [genres]="genres" 
	(saveMovie)="saveMovie($event)" (cancelMovie)="cancelMovie()" (deleteMovie)="deleteMovie($event)" 
	[movie]="selectedMovie">
</app-add-movie>
```
    
We can see that there are [Inputs] and (Outputs) defined as well as [(banana-in-the-box)] two-way binding.
* Inputs - The AddMovieComponent movie Input property is set to the selectedMovie (this was set from MovieVirtualListComponent)
         The list of genres is also passed in as an input.
* Outputs - The AddMovieComponent saveMovie, cancelMovie, deleteMovie are outputs triggered by user interacting with the AddMovieComponent.
* Two-way binding - The AddMovieComponent is actually used for add, edit and viewing Movie information which is customised depending on the mode of operation.
These modes are both inputs and outputs, allowing the component to transition from view to edit mode for example and vice-versa. Strictly speaking the add mode is an input only, as there is no transition to view/edit modes from the add mode.

## Services and Pipes
### MovieService
This provides the interface to the REST API and provides a 1:1 mapping to those Http calls.

### Toast Service
Implements pop-up notifications in the top right corner of application. Currently displayed success (green) notifications for Add Movie, Edit Movie and Delete Movie,
and displayed error (red) notifications for HttpErrors - for instance if the REST API endpoint is not available.

### ColorMap service
Generates random colors in the form '#rrggbb' (where rr, gg, bb are hex values between 00 and ff).  
The colors are mapped to the name provided (case insensitive), such that any subsequent request for a color for the same name will return the same colour.    
The colormap service is used to maintain the same avatar background color for all genres of the same name.   

### Pipes
Created and use a few pipes to help with data formatting. For instance,  
* ReplaceNbspPipe is used to replace the `&nbsp;` character that was seeing on some Movie titles, which was stopping the title from being wrapped and split over multiple lines (e.g. in the View/Edit Panels). 
* InitialsPipe is used to create a better Avatar string. The default behaviour would pick the first characters of each word in the titles, which often meant more than two characters. Instead, this picks out only the capital letters of the words and uses the first and last letters of the result. If the movie title is all lowercase then lowercase characters will be used.
* TruncatePipe will truncate a test string to maximum number of characters with ellipsis '...' appended to the end.  


# The Pipeline

The pipeline provides the automation of build and deployment scripts whenever there is an update to the repository. So with any commit or push to GitLab, the CI/CD pipeline is triggered into action.  

A [Basic Gitlab Pipeline](https://docs.gitlab.com/ee/ci/pipelines/pipeline_architectures.html) is implemented here, and is simplified into two stages; a build stage and a deploy stage. Everything is run in the build stage concurrently, then everything is run in the deploy stage.  

The pipeline is defined by the .gitlab-ci.yml plus some additional files. 

* Dockerfile - builds and deploys the Angular Web Application
* deployment.yml - defines the deployment
* ingresswebfrontend.yaml - configures the Kubernetes ingress controller

## .gitlab-ci.yml
   
The Gitlab CI/CD pipeline is configured with the .gitlab-ci.yml YAML file. This file is based on a .gitlab-ci.yml [Docker Template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Docker.gitlab-ci.yml) for the build stage; one of the many [example templates](https://docs.gitlab.com/ee/ci/examples/#cicd-templates)  

<details><summary>View .gitlab-ci.yml</summary>

```docker-build:  
stages:
    - build
    - deploy

docker-build:
    # Official docker image.
    image: docker:latest
    stage: build
    services:
    - docker:dind
    before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    script:
    - docker build --pull --build-arg BUILD_ARG="$CI_COMMIT_SHORT_SHA" -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG" -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA" -t "$CI_REGISTRY_IMAGE:latest" .
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA"
    - docker push "$CI_REGISTRY_IMAGE:latest"
    

docker-deploy:
    image: 
      name: cnad/kubectl:latest
      entrypoint: [""]
    stage: deploy
    script:
      - export KUBECONFIG=$kube_config
      - kubectl apply -f deployment.yaml 
      - kubectl rollout restart deployments/kubemoviewebfront 
      - kubectl apply -f ingresswebfrontend.yaml
    environment:
      name: staging
      url: https://52.170.3.117:16443

```

</details>

### docker-build: (variations to the Docker template)
The docker-build mostly follows the template with some minor additions
* build - multiple tags are applied to the built image as follows:
	* $CI_COMMIT_REF_SLUG
	* $CI_COMMIT_SHORT_SHA
	* latest

	The CI_ variables are [predefined Gitlab environment variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) essentially used here to tag the build with a commit identify.

	Additionally, the build-arg option is used to set build-time variables which can be referenced from the Dockerfile. This is a custom setting used to update the image environment with a Gitlab Commit reference. This is required to allow the deployed web application to display build information as a way to confirm the source image of the deployment. (Refer to Dockerfile below for further details).

* push - the image tagged latest and CI_COMMIT_SHORT_SHA are pushed (whereas the template does not push tagged image)

### docker-deploy
For the deploy stage the built image is deployed to a [Kubernetes cluster](reference to the Kubernetes cluster goes here) using kubectl command and deployment YAML. The kubectl command is made available via the [cnad/kubectl](https://gitlab.com/cnad/kubectl) Docker image.  

GitLab's CI/CD variables are used to store cluster authentication information which is made available to kubectl via the KUBECONFIG environment variable; the environment variable is set as part of the docker-deploy job from the kube_config CI/CD variable. The deployment YAML (deployment.yaml) is then used to deploy the Web front end image to the Kubernetes cluster, restart the resource and to configure the cluster Ingress.  

## Dockerfile

<details><summary>View Dockerfile</summary>

```
### STAGE 1: Build ###
FROM node:14.5.0-alpine AS build
ARG BUILD_ARG=0
WORKDIR /usr/src/app
COPY /KubeMovie .
RUN npm install
RUN npm run update-build-prod -- $BUILD_ARG
RUN npm run build-prod

### STAGE 2: Run ###
FROM nginx:1.19.0-alpine
COPY --from=build /usr/src/app/dist/KubeMovie /usr/share/nginx/html
```
</details>

The Dockerfile is used in the docker build command run by the pipeline (see .gitlab-ci.yml) - noting that Dockerfile is the default name and therefore not required in the build command.  

The Dockerfile defines a multi-stage build comprising a build of the KubeMovie web application (Stage 1) and then this built artifact is copied to Stage 2. The result is an Nginx web server image containing the web application. 

The Stage 1 build involves, installing dependencies, updating the environment and building a production image.

The ARG instruction defines the BUILD_ARG build-time variable which defaults to 0. This variable is set externally using the --build-arg option when performing the docker build. The variable is only available up until when the image is built. The variable is passed to the update-build-prod script, which is a custom script of the application that will set the 'Build' production environment variable. This is used at runtime by the application to display the GitLab commit identifier.

## deployment.yaml
<details><summary>View deployment.yaml</summary>

```
apiVersion: v1
kind: Service
metadata:
  name: kubemoviewebfront
  labels:
      app: kubemoviewebfront
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: kubemoviewebfront
  type: LoadBalancer
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: kubemoviewebfront
  labels:                     # Labels that will be applied to this resource
    app: kubemoviewebfront
spec:
  replicas: 1
  selector:
    matchLabels:
      app: kubemoviewebfront
  template:
    metadata:
      labels:
        app: kubemoviewebfront
    spec:
      containers:
      - name: kubemoviewebfront
        image: registry.gitlab.com/cnad/kubedvdangular:latest
        imagePullPolicy: Always
        ports:
          - name: http
            containerPort: 80
      imagePullSecrets:
      - name: gitcreds


```
</details>

This YAML file is used by .gitlab-ci.yml to create a deployment to rollout a replica set to the Kubernetes cluster. There are two components to this deployment; a front-end Service object and a backend Deployment. The Service object is used to expose the backend replica set on the network. This is a common deployment model to abstract the running POD(s) to a single cluster IP. This decoupling of cluster IP from the running POD(s) allows the POD(s) to die and get replicated without affecting consumers. This deployment currently only deploys a replica set of 1, but could easily be configured with say 3 or more replicas and because the Service is a LoadBalancer it will also distribute the traffic to balance load across the Web Application POD(s).

## ingresswebfrontend.yaml

<details><summary>View ingresswebfrontend.yaml</summary>

```
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: frontend-ingress
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - host: devk8subuntu.eastus.cloudapp.azure.com
    http:
      paths:
      - path: /
        backend:
          serviceName: kubemoviewebfront 
          servicePort: 80
```
</details>

This YAML file is used by .gitlab-ci.yml to configure the Kubernetes Nginx ingress controller. By default pods of Kubernetes services are not accessible from the external network, but only by other pods within the Kubernetes cluster. The ingress configuration provides rules to expose the kubemoviewebfront service resource previously described. In this case the service is exposed on the root path of the cluster on port 80 (http://devk8subuntu.eastus.cloudapp.azure.com/)





