### STAGE 1: Build ###
FROM node:14.5.0-alpine AS build
ARG BUILD_ARG=0
WORKDIR /usr/src/app
COPY /KubeMovie .
RUN npm install
RUN npm run update-build-prod -- $BUILD_ARG
RUN npm run build-prod

### STAGE 2: Run ###
FROM nginx:1.19.0-alpine
COPY --from=build /usr/src/app/dist/KubeMovie /usr/share/nginx/html