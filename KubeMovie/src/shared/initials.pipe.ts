import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'initials'
})

// used for avatar-ngx to return initials as a way to limit length of avatar string
// initially takes the first and last initials, but could be extended to be parameterised
export class InitialsPipe implements PipeTransform {
  transform(value: string) {

    const anyInitials = value?.match(/\b(\w)/g)?.toLocaleString().replace(/[, ]/g, '');
    const capInitials = anyInitials?.replace(/[a-z]/g, '');
    const initials = capInitials?.length > 0 ? capInitials : anyInitials;

    return (initials && (initials.length > 0)) ?
      initials.slice(0, 1) + initials.slice(Math.max(1, initials.length - 1), initials.length) :
      '?';
  }
}
