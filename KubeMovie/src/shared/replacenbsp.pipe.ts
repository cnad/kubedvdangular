import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'replaceNbsp'
})

export class ReplaceNbspPipe implements PipeTransform {
    transform(value: string, replacement: string = ' ') {

        value = value.replace(/\u00A0/g, replacement);
        return value;
    }
}
