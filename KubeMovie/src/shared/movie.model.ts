export interface IMovie {
    id: number;
    title: string;
    genre: string;
}

export interface IKubeMovieControllerVersion {
    kubemoviecontrollerversion: string;
}
