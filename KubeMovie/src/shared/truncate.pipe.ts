import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'truncate'
})


export class TruncatePipe implements PipeTransform {
  transform(value: string, length: number) {

    return value.length > length ?
        value.substr(0, length - 3) + '...' :
        value;
  }
}
