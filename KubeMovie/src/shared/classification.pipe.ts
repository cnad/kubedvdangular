import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'classification'})
export class ClassificationPipe implements PipeTransform {
    transform(value: string): string {
        switch(value) {
            case 'ctc': return '../assets/images/classification-ctc-square.png';
            case 'g': return '../assets/images/classification-g-square.png';
            case 'pg': return '../assets/images/classification-pg-square.png';
            case 'ma15': return '../assets/images/classification-ma15-square.png';
            case 'm': return '../assets/images/classification-m-square.png';
            case 'r18': return '../assets/images/classification-r18-square.png';
            case 'x18': return '../assets/images/classification-x18-square.png';
            default: return '../assets/images/classification-g-square.png';
        }
    }
}
