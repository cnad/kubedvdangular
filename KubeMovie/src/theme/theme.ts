export interface Theme {
    name: string;
    properties: any;
  }

export const light: Theme = {
    name: 'light',

    properties: {
      '--km-primary-foreground-color' : 'black',
      '--km-primary-background-color' : 'white',
      '--km-secondary-foreground-color' : 'black',
      '--km-secondary-background-color' : '#e7e7e7',
      '--km-tertiary-foreground-color' : 'black',
      '--km-tertiary-background-color' : '#bebebe',
      '--km-navbar-foreground-color' : 'white',
      '--km-navbar-background-color' : '#4472c4',
  }
  };

export const dark: Theme = {
    name: 'dark',
    properties: {
        '--km-primary-foreground-color' : 'whitesmoke',
        '--km-primary-background-color' : '#343a40',
        '--km-secondary-foreground-color' : 'whitesmoke',
        '--km-secondary-background-color' : '#48515a',
        '--km-tertiary-foreground-color' : 'whitesmoke',
        '--km-tertiary-background-color' : '#48515a',
        '--km-navbar-foreground-color' : 'whitesmoke',
        '--km-navbar-background-color' : 'black',
    }
  };
