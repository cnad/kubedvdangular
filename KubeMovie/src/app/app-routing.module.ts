import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MovieCatalogComponent } from 'src/movies/movie-catalog.component';
import { MovieResolver } from 'src/movies/movie-resolver.service';
import { HomeComponent } from './home.component';


const routes: Routes = [
  { path: '', component: MovieCatalogComponent, resolve: { movies: MovieResolver } }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
