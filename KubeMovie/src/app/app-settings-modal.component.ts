import {Component, OnInit} from '@angular/core';

import {NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { environment } from 'src/environments/environment';
import { MovieService } from 'src/movies/movie.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastService } from 'src/common/toast.service';
import { timer } from 'rxjs';

@Component({
  selector: 'app-settings-modal',
  templateUrl: './app-settings-modal.component.html',

})
export class AppSettingsModalComponent implements OnInit {

  closeResult = '';
  kubemoviebuild = '';
  kubemovieapibuild = '';
  loading = false;

  constructor(
    private movieService: MovieService,
    private toastService: ToastService,
    public activeModal: NgbActiveModal) {}

  ngOnInit(): void {

    this.loading = true;
    this.kubemoviebuild = environment.build;
    this.kubemovieapibuild = 'loading...';

    this.movieService.getVersion().subscribe((v) => {
      if (v instanceof HttpErrorResponse) {
          this.toastService.showHttpError(v);
      } else {
        this.kubemovieapibuild = v?.kubemoviecontrollerversion?.length > 0 ? v.kubemoviecontrollerversion : 'not available';
      }
      this.loading = false;

    });

  }

//   open(content) {
//     this.modalService.open(content, {ariaLabelledBy: 'app-settings-modal-title'}).result.then((result) => {
//       this.closeResult = `Closed with: ${result}`;
//     }, (reason) => {
//       this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
//     });
//   }

  private getDismissReason(reason: any): string {

    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }

  }
}
