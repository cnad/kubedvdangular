import { Component, OnInit } from '@angular/core';
import { ToastService } from 'src/common/toast.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'KubeMovie';

  ngOnInit() {
  }

  constructor(public toastService: ToastService) {
  }
}
