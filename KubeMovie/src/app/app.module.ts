import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AvatarModule } from 'ngx-avatar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NavBarComponent } from 'src/nav/navbar.component';
import { MovieService } from 'src/movies/movie.service';
import { MovieCatalogComponent } from 'src/movies/movie-catalog.component';
import { HttpClientModule } from '@angular/common/http';
import { MovieResolver } from 'src/movies/movie-resolver.service';
import { ClassificationPipe } from 'src/shared/classification.pipe';
import { HomeComponent } from './home.component';
import { AddMovieComponent } from 'src/movies/add-movie.component';
import { InitialsPipe } from 'src/shared/initials.pipe';
import { TruncatePipe } from 'src/shared/truncate.pipe';
import { ToastService } from 'src/common/toast.service';
import { ToastComponent } from 'src/common/toast.component';
import { MovieVirtualListComponent } from 'src/movies/movie-virtual-list.component';
import { VirtualScrollerModule } from 'ngx-virtual-scroller';
import { AppSettingsModalComponent } from './app-settings-modal.component';
import { ColorMapService } from 'src/common/colormap.service';
import { ReplaceNbspPipe } from 'src/shared/replacenbsp.pipe';
import { ThemeService } from 'src/theme/theme.service';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    MovieCatalogComponent,
    MovieVirtualListComponent,
    ClassificationPipe,
    InitialsPipe,
    TruncatePipe,
    ReplaceNbspPipe,
    HomeComponent,
    AddMovieComponent,
    ToastComponent,
    AppSettingsModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    NgbModule,
    FontAwesomeModule,
    HttpClientModule,
    AvatarModule,
    VirtualScrollerModule
  ],
  providers: [
    MovieService,
    ToastService,
    ColorMapService,
    ThemeService,
    MovieResolver
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
