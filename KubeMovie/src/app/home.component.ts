import { Component } from '@angular/core';

@Component({
    template: `
        <h2>
        <a href="https://gitlab.com/cnad/kubemovieangular/-/wikis/home">Visit the application wiki for details about KubeMovie</a>
        </h2>
    `,
    styles: [`
        .container { padding-left:20px; padding-right:20px; }
        .movie-catalog-image { height: 100px; }
        a {cursor:pointer}
    `]
})

export class HomeComponent {

}
