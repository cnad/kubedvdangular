import { Injectable, TemplateRef } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class ToastService {
  toasts: any[] = [];

  show(textOrTpl: string | TemplateRef<any>, options: any = {}) {

    this.toasts.push({ textOrTpl, ...options });
  }

  remove(toast) {
    this.toasts = this.toasts.filter(t => t !== toast);
  }

  showSuccess(message: string, header: string = 'Error') {
    this.show(message, {
      classname: 'bg-success text-light',
      delay: 2000 ,
      autohide: true,
      headertext: header
    });
  }

  showInfo(message: string, header: string = 'Information') {
    this.show(message, {
      classname: 'bg-info text-light',
      delay: 2000 ,
      autohide: true,
      headertext: header
    });
  }

  showError(message: string, header: string = 'Error') {
    this.show(message, {
      classname: 'bg-danger text-light',
      delay: 5000 ,
      autohide: true,
      headertext: header
    });
  }

  showHttpError(error: HttpErrorResponse, ) {
    this.show(error.message, {
      classname: 'bg-danger text-light',
      delay: 5000 ,
      autohide: true,
      headertext: 'Service Error'
    });
  }

}