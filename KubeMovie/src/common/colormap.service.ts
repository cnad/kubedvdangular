import { Injectable, TemplateRef } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ColorMapService {
  private colormap = new Map<string, string>();

  private getRandomColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  getColor(item: string) {

    if (!item) {
      return '#343a40';
    }

    const name = item.toLocaleLowerCase();
    let color = this.colormap.get(name);
    if (!color) {
      color = this.getRandomColor();
      this.colormap.set(name, color);
      //console.log('adding ' + name + ' : ' + color + ' to color map');
    }

    if (!color) { console.error('No color found for ' + name); }
    return color;
}

}
