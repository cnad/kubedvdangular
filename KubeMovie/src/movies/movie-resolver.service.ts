import { Injectable } from '@angular/core';
import { Resolve, RouteConfigLoadEnd, ActivatedRouteSnapshot } from '@angular/router';
import { map, tap } from 'rxjs/operators';
import { MovieService } from './movie.service';
import { environment } from 'src/environments/environment';

@Injectable()
export class MovieResolver implements Resolve<any> {
    constructor(private movieService: MovieService) {

    }
        resolve(route: ActivatedRouteSnapshot) {
            const title = route.paramMap.get('title');
            const genre = route.paramMap.get('genre');
            // console.log('MovieResolver.resolver: title=' + title + ' genre=' + genre);

            return this.movieService.getMovies(genre, title, 1, environment.pagesize);
        }

}