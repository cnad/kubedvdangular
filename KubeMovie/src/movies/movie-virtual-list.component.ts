import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { MovieService } from './movie.service';
import { IMovie } from 'src/shared/movie.model';
import { faEdit, faFilter, faCog, faPlus, faSearch } from '@fortawesome/free-solid-svg-icons';
import { VirtualScrollerComponent, IPageInfo } from 'ngx-virtual-scroller';
import { ColorMapService } from 'src/common/colormap.service';
import { isNumber } from 'util';

@Component({
    selector: 'app-movie-virtual-list',
    templateUrl: './movie-virtual-list.component.html',
    // styles: [`
    //     .container { padding-left:20px; padding-right:20px; }
    //     .movie-catalog-image { height: 100px; }
    //     a {cursor:pointer}
    // `]
})

export class MovieVirtualListComponent implements OnInit, OnChanges {
    faEdit = faEdit;
    faFilter = faFilter;
    faCog = faCog;
    faPlus = faPlus;
    faSearch = faSearch;
    @Input() selectedMovie: IMovie = null;
    @Input() loading: boolean;

    @Input() movies: IMovie[];
    @Output() selectMovie = new EventEmitter<IMovie>();
    @Output() fetchNextChunk = new EventEmitter<IPageInfo>();
    displayedMovies: IMovie[] = [];

    @ViewChild('scroll') scroller: VirtualScrollerComponent;

    constructor(private movieService: MovieService, private colorMapService: ColorMapService) {
    }

    getColor(genre: string) {
        return this.colorMapService.getColor(genre);
    }

    isNumber(value) {
        return !isNaN(Number(value));
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.displayedMovies = this.movies;
    }

    ngOnInit() {
        this.displayedMovies = this.movies;
    }

    editMovie(movie: IMovie) {
        this.selectedMovie = movie;
        this.selectMovie.emit(movie);
    }

    onVsUpdate(event) {
        // console.log('vs update', event);
      }

    onVsChange(event) {
        // console.log('vs change', event);
    }

    onVsStart(event) {
        // console.log('vs start', event);
    }

    onVsEnd(event) {
        // console.log('vs end', event);
        this.fetchNextChunk.emit(event);
    }


}
