import { Injectable, EventEmitter } from '@angular/core';
import { Subject, Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { IMovie, IKubeMovieControllerVersion } from 'src/shared/movie.model';
import { environment } from '../environments/environment';

@Injectable()
export class MovieService {

    private baseUrl = environment.baseurl;

    constructor(private http: HttpClient) {
    }

    saveMovie(movie: IMovie) {
        if (movie.id) {
            return this.updateMovie(movie);
        } else {
            return this.addMovie(movie);
        }
    }

    addMovie(movie: IMovie): Observable<IMovie> {

        const options = { headers : new HttpHeaders({'Content-Type': 'application/json'})};
        return this.http.post<IMovie>(this.baseUrl + '/api/movie/createmovie/', movie, options)
            .pipe(catchError(this.handleError<IMovie>('addMovie')));
    }

    updateMovie(movie: IMovie): Observable<IMovie> {

        const options = { headers : new HttpHeaders({'Content-Type': 'application/json'})};
        return this.http.put<IMovie>(this.baseUrl + '/api/movie/updatemovie/' + movie.id, movie, options)
            .pipe(catchError(this.handleError<IMovie>('updateMovie')));
    }

    deleteMovie(movie: IMovie): Observable<any> {

        const options = { headers : new HttpHeaders({'Content-Type': 'application/json'})};

        return this.http.delete(this.baseUrl + '/api/movie/deletemovie/' + movie.id)
            .pipe(catchError(this.handleError<any>('deletemovies')));
    }

    getMovies(title: string, genre: string, pagenumber: number= 1, pagesize: number= environment.pagesize): Observable<IMovie[]> {

        let params = new HttpParams();
        params = genre?.length > 0 ? params.append('genre', genre) : params;
        params = title?.length > 0 ? params.append('title', title) : params;
        params = params.append('pagesize', pagesize.toString());
        params = params.append('pagenumber', pagenumber.toString());

        return this.http.get<IMovie[]>(this.baseUrl + '/api/movie/getmovies', {params})
            .pipe(catchError(this.handleError<IMovie[]>('getmovies')));
    }

    getMovie(id: number): Observable<IMovie> {

        let params = new HttpParams();
        params = params.append('fields', 'id,title,genre');

        return this.http.get<IMovie>(this.baseUrl + '/api/movie/getmovie/' + id.toString(), {params})
            .pipe(catchError(this.handleError<IMovie>('getmovie')));
    }


    getGenres(): Observable<string[]> {

        return this.http.get<string[]>(this.baseUrl + '/api/movie/getgenres')
            .pipe(catchError(this.handleError<string[]>('getgenres')));
    }

    getVersion(): Observable<IKubeMovieControllerVersion> { 

        let params = new HttpParams();
        params = params.append('section', 'KubeMovieControllerVersion');

        return this.http.get<IKubeMovieControllerVersion>(this.baseUrl + '/api/movie/getenv', {params})
            .pipe(catchError(this.handleError<IKubeMovieControllerVersion>('getenv')));
    }

    // template to handle basic errors - log to console
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            return result ? of(result as T) : of(error);
        };
    }
}

