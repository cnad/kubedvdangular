import { Component, OnInit, Input, OnChanges, Output } from '@angular/core';
import { MovieService } from './movie.service';
import { IMovie } from 'src/shared/movie.model';
import { ActivatedRoute, Router } from '@angular/router';
import { faSearch, faCog, faPlus, faTimes, faEdit, faFilter, faWindowClose } from '@fortawesome/free-solid-svg-icons';
import { FormGroup } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastService } from 'src/common/toast.service';
import { IPageInfo } from 'ngx-virtual-scroller';
import { AppSettingsModalComponent } from 'src/app/app-settings-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ColorMapService } from 'src/common/colormap.service';


@Component({
    templateUrl: './movie-catalog.component.html',
    styles: [`
        .container { padding-left:20px; padding-right:20px; }
        .movie-catalog-image { height: 100px; }
        a {cursor:pointer}
    `]
})

export class MovieCatalogComponent implements OnInit {
    @Input() movies: IMovie[];
    public addmode = false;
    public editmode = false;
    public viewmode = false;
    selectedMovie: IMovie;
    filterBy = 'all';
    sortBy = 'all';
    faPlus = faPlus;
    faEdit = faEdit;
    faFilter = faFilter;
    faCog = faCog;
    faSearch = faSearch;
    faWindowClose = faWindowClose;
    faTimes = faTimes;
    searchTerm = '';
    genre = '';
    genres: string[];
    dropdownGenres: string[];
    page = 2;
    loading = false;
    saving = false;
    deleting = false;


    constructor(
        private movieService: MovieService,
        private route: ActivatedRoute,
        private router: Router,
        private toastService: ToastService,
        private modalService: NgbModal,
        private colorMapService: ColorMapService) {

    }

    ngOnInit() {
        this.movies = this.route.snapshot.data.movies;
        this.movieService.getGenres().subscribe((g) => {
            if (g instanceof HttpErrorResponse) {
                this.toastService.showHttpError(g);
            } else {
                g.sort((a, b) => a.localeCompare(b));
                this.genres = g;
                this.dropdownGenres = g.slice(0);
            }
        });
    }

    clearSearch() {
        this.searchTerm = '';
        this.getMovies();

    }

    clearFilter() {
        if (this.genre?.length > 0) {
            this.genre = '';
            this.getMovies();
        }

    }

    showMovieView() {
        this.addmode = false;
        this.editmode = false;
        this.viewmode = true;
    }

    hideMovieModes() {
        this.addmode = false;
        this.editmode = false;
        this.viewmode = false;
    }

    addMovie() {
        this.addmode = true;
        this.editmode = false;
        this.viewmode = false;
        this.selectedMovie = null;
    }

    selectMovie(movie) {
        this.getMovie(movie?.id);
    }

    saveMovie(movie: IMovie) {

        this.movieService.saveMovie(movie).subscribe( d => {
            if (d instanceof HttpErrorResponse) {
                this.toastService.showHttpError(d);
            } else {
                this.toastService.showSuccess((d ? 'New movie successfully added' : 'Movie changes saved'));
                if (d && (!this.genre || d.genre === this.genre || this.genre?.length === 0)) {
                    this.movies.unshift(d);
                    this.movies.sort((a, b) => a.title.localeCompare(b.title));
                    this.selectedMovie = d;
                }

                this.showMovieView();

            }

            this.saving = false;
        });
    }

    deleteMovie(movie: IMovie) {
        this.deleting = true;
        this.movieService.deleteMovie(movie).subscribe( resp => {
            if (resp instanceof HttpErrorResponse) {
                this.toastService.showHttpError(resp);
            } else {
                this.toastService.showSuccess(movie.title + ' has been deleted');
                this.movies = this.movies.filter(d => d.id !== movie.id);

                this.hideMovieModes();
            }

            this.deleting = false;
        });
    }

    cancelMovie() {
        this.viewmode = this.editmode;
        this.addmode = false;
        this.editmode = false;
    }

    dropdownClick(genre) {
        this.genre = genre;
        this.getMovies();
    }

    fetchNextChunk(event: IPageInfo) {

        // console.log('movies = ' + this.movies.length);
        if (event.endIndex !== this.movies.length - 1) { return; }
        if (this.loading) { return; }

        // console.log('fetch chunk page=' + this.page);

        this.loading = true;
        this.movieService.getMovies(this.searchTerm, this.genre, this.page).subscribe(movies => {
            if (movies instanceof HttpErrorResponse) {
                this.toastService.showHttpError(movies);
            } else {
                if (movies.length === 0) {
                    // this.toastService.showInfo('No more movies ' + this.movies.length);
                    // console.log('No more movies ' + this.movies.length);
                } else {
                    this.page += 1;
                    this.movies =  this.movies.concat(movies);
                    // this.toastService.showSuccess('Retrieved page ' + this.page + ' Movies = ' + this.movies.length);
                    // console.log('fetchNextChunk: Retrieved page ' + this.page + ' Movies = ' + this.movies.length);

                    // for (const d of movies) {
                    //     console.log(d.title);
                    // }
                }
                this.loading = false;
            }
        });
    }

    getMovies() {
        this.page = 1;
        this.movies = [];
        this.loading = true;

        this.movieService.getMovies(this.searchTerm, this.genre, this.page).subscribe(movies => {
            if (movies instanceof HttpErrorResponse) {
                console.error('MovieCatalogComponent.getMovies HttpErrorResponse ' + movies.status);
                this.toastService.showHttpError(movies);
            } else {
                // this.toastService.showSuccess('Retrieved page ' + this.page);
                this.movies = movies;
                this.page += 1;
                // console.log('getMovies: Retrieved page ' + this.page + ' Movies = ' + this.movies.length);
            }
            this.loading = false;
        });
    }

    getMovie(id: number) {

        if (id) {
            this.loading = true;
            this.movieService.getMovie(id).subscribe(movie => {
                if (movie instanceof HttpErrorResponse) {
                    console.error('MovieCatalogComponent.getMovie HttpErrorResponse ' + movie.status);
                    this.toastService.showHttpError(movie);
                } else {
                    this.editmode = false;
                    this.addmode = false;
                    this.viewmode = true;
                    this.selectedMovie = movie;
                }
                this.loading = false;
            });
        } else {
            this.editmode = false;
            this.addmode = false;
            this.viewmode = false;
            this.selectedMovie = null;
        }

    }

    editSettings() {
        this.modalService.open(AppSettingsModalComponent);

    }
}
