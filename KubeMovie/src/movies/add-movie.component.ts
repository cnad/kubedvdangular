import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChange, SimpleChanges, Type } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { IMovie } from 'src/shared/movie.model';
import { ConditionalExpr } from '@angular/compiler';
import { faTrashAlt, faEdit } from '@fortawesome/free-solid-svg-icons';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ColorMapService } from 'src/common/colormap.service';


@Component({
    selector: 'app-add-movie',
    templateUrl: './add-movie.component.html',
    styles: [`
    em { float:right; color:#E05C65; padding-left:10px; }
    .error input, .error select, .error textarea {background-color:#E3C3C5;}
    .error ::-webkit-input-placeholder { color: #999; }
    .error ::-moz-placeholder { color: #999; }
    .error :-moz-placeholder { color: #999; }
    .error ::-ms-input-placeholder { color: #999; }
    `]
})

export class AddMovieComponent implements OnInit, OnChanges {

    @Input() viewMode = false;
    @Input() editMode = false;
    @Input() addMode = false;
    @Output() viewModeChange = new EventEmitter<boolean>();
    @Output() editModeChange = new EventEmitter<boolean>();
    @Output() addModeChange = new EventEmitter<boolean>();

    @Output() saveMovie = new EventEmitter();
    @Output() cancelMovie = new EventEmitter();
    @Output() deleteMovie = new EventEmitter();
    @Input() movie: IMovie;
    @Input() genres;

    panelName: string;
    public addMovieForm: FormGroup;
    faTrash = faTrashAlt;
    faEdit = faEdit;

    title: FormControl;
    genre: FormControl;
    classification: FormControl;
    duration: FormControl;
    closeResult: string;

    constructor(private colorMapService: ColorMapService, public modal: NgbModal) {
    }


    ngOnInit(): void {
        this.title = new FormControl(this.movie?.title, Validators.required);
        this.genre = new FormControl(this.movie?.genre, Validators.required);

        this.addMovieForm = new FormGroup({
            title: this.title,
            genre: this.genre,
        });

        this.panelName = this.movie ? 'Edit Movie' : 'Add Movie';
    }

    ngOnChanges(changes: SimpleChanges): void {
        this.panelName = 'Edit Movie';
        this.title?.setValue(this.movie?.title);
        this.genre?.setValue(this.movie?.genre);
    }

    getColor(genre: string) {
        return this.colorMapService.getColor(genre);
    }

    save(formValues) {

        if (!this.movie) {
            const movie: IMovie = {
                id: undefined,
                title: formValues.title,
                genre: formValues.genre,
            };
            this.saveMovie.emit(movie);
        } else {
            this.movie.genre = formValues.genre;
            this.movie.title = formValues.title;

            this.saveMovie.emit(this.movie);
        }

    }

    delete() {
        this.deleteMovie.emit(this.movie);

        // this.modal.open('Confirm delete?', {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
        //     this.closeResult = `Closed with: ${result}`;
        //   }, (reason) => {
        //     this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        //   });
    }

    editMovie() {
        this.viewModeChange.emit(false);
        this.editModeChange.emit(true);
    }
    getDismissReason(reason: any) {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
          } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
          } else {
            return `with: ${reason}`;
          }
    }

    cancel() {
        this.cancelMovie.emit();
    }


}
