import { Component } from '@angular/core';
import { faSearch, faBell, faUser, faHome, faCog, faPlus, faEdit, faFilm, faLightbulb } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { MovieService } from 'src/movies/movie.service';
import { IMovie } from 'src/shared/movie.model';
import { ThemeService } from 'src/theme/theme.service';

@Component({
    selector: 'app-nav-bar',
    templateUrl: './navbar.component.html',
    styles: [`
        .nav.navbar-nav {font-size: 15px;}
        #searchForm {margin-right: 100px;}
        @media (max-width: 1200px) {#searchForm {display:none}}
        li > a.active { color: #F97924 }
    `]
})

export class NavBarComponent {
    faSearch = faSearch;
    faBell = faBell;
    faUser = faUser;
    faHome = faHome;
    faTools = faCog;
    faPlus = faPlus;
    faEdit = faEdit;
    faFilm = faFilm;
    faLightbulb = faLightbulb;
    searchTerm = '';
    foundMovies: IMovie[];

    constructor(private movieService: MovieService, private router: Router, private themeService: ThemeService) {
    }

    toggleTheme() {
        if (this.themeService.isDarkTheme()) {
            this.themeService.setLightTheme();
        } else {
            this.themeService.setDarkTheme();
        }
    }
}
