
var replace = require('replace-in-files');
var buildVersion = process.argv[2];
const options = {
    files: 'src/environments/environment.prod.ts',
    from: /build: '(.*)'/g,
    to: "build: '"+ buildVersion + "'",
    allowEmptyPaths: false,
};

replace(options)
  .then(({ changedFiles, countOfMatchesByPaths }) => {
    console.log('Build: ' + buildVersion);
  })
  .catch(error => {
    console.error('Error occurred:', error);
  });